package com.example.manuel.examplekodein.featureone.repository

import com.example.manuel.examplekodein.featureone.repository.datasource.FeatureOneDatasource

class FeatureOneRepositoryImpl(private val datasource: FeatureOneDatasource) : FeatureOneRepository {

    override fun getData() = datasource.getData()
}