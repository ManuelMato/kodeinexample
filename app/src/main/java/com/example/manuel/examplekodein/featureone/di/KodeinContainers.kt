package com.example.manuel.examplekodein.featureone.di

import com.example.manuel.examplekodein.featureone.domain.usecase.FeatureOneUseCase
import com.example.manuel.examplekodein.featureone.repository.FeatureOneRepository
import com.example.manuel.examplekodein.featureone.repository.FeatureOneRepositoryImpl
import com.example.manuel.examplekodein.featureone.repository.datasource.FeatureOneDatasource
import com.example.manuel.examplekodein.featureone.vm.FeatureOneViewModel
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.provider

class KodeinContainers {

    companion object {
        val architectureDependencies = Kodein {
            bind<FeatureOneDatasource>() with provider { FeatureOneDatasource() }
            bind<FeatureOneRepository>() with provider { FeatureOneRepositoryImpl(instance()) }
            bind<FeatureOneUseCase>() with provider { FeatureOneUseCase(instance()) }
            bind<FeatureOneViewModel>() with provider { FeatureOneViewModel(instance()) }
        }
    }
}