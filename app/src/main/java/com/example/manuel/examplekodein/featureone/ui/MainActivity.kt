package com.example.manuel.examplekodein.featureone.ui

import android.arch.lifecycle.Observer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.manuel.examplekodein.featureone.di.KodeinContainers
import com.example.manuel.examplekodein.featureone.vm.FeatureOneViewModel
import com.example.manuel.examplekodein.R
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.newInstance

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: FeatureOneViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initViewModel()
        initObserver()
    }

    private fun initViewModel() {
        viewModel =
                KodeinContainers.architectureDependencies.newInstance {
                    FeatureOneViewModel(instance())
                }
    }

    private fun initObserver() {
        viewModel.getData()
                .observe(this, Observer<String> { data -> onDataReceived(data) })
    }

    private fun onDataReceived(data: String?) {
        Log.i("test", data)
    }
}
