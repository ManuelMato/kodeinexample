package com.example.manuel.examplekodein.featureone.domain.usecase

import com.example.manuel.examplekodein.featureone.repository.FeatureOneRepository

class FeatureOneUseCase(private val repository: FeatureOneRepository) {

    fun execute() = repository.getData()
}