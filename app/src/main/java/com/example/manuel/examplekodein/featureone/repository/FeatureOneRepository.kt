package com.example.manuel.examplekodein.featureone.repository

interface FeatureOneRepository {

    fun getData(): String
}