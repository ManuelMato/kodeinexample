package com.example.manuel.examplekodein.featureone.vm

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.example.manuel.examplekodein.featureone.domain.usecase.FeatureOneUseCase

class FeatureOneViewModel(private val useCase: FeatureOneUseCase) : ViewModel() {

    private val dataLiveData = MutableLiveData<String>()

    fun getData(): MutableLiveData<String> {
        dataLiveData.value = useCase.execute()

        return dataLiveData
    }
}