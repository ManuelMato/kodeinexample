package com.example.manuel.examplekodein.featureone.domain.usecase

import com.example.manuel.examplekodein.featureone.repository.FeatureOneRepository
import com.github.salomonbrys.kodein.*
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import org.junit.Test

class FeatureOneUseCaseTest {

    private val mockFeatureOneRepositoryImpl: FeatureOneRepository = mock()

    val architectureDependenciesTest = Kodein {
        bind<FeatureOneRepository>() with provider { mockFeatureOneRepositoryImpl }
    }

    val featureOneUseCase = architectureDependenciesTest.newInstance {
        FeatureOneUseCase(instance())
    }

    @Test
    fun verifyUseCaseExecuteCallOneTimeGetDataRepository() {
        featureOneUseCase.execute()

        verify(mockFeatureOneRepositoryImpl, times(1)).getData()
    }
}