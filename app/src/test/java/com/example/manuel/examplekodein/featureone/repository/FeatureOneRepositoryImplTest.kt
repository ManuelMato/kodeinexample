package com.example.manuel.examplekodein.featureone.repository

import com.example.manuel.examplekodein.featureone.repository.datasource.FeatureOneDatasource
import com.github.salomonbrys.kodein.*
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import org.junit.Test

open class FeatureOneRepositoryImplTest {

    private val mockFeatureOneDatasource: FeatureOneDatasource = mock()

    private val architectureDependenciesTest = Kodein {
        bind<FeatureOneDatasource>() with provider { mockFeatureOneDatasource }
    }

    private val featureOneRepositoryImpl = architectureDependenciesTest.newInstance {
        FeatureOneRepositoryImpl(instance())
    }

    @Test
    fun verifyRepositoryGetDataCallOneTimeGetDataDatasource() {
        featureOneRepositoryImpl.getData()

        verify(mockFeatureOneDatasource, times(1)).getData()
    }
}